import 'dart:async';

import 'package:alarm/alarm.dart';
import 'package:flutter/material.dart';
import 'notification_helper.dart';

class TestNotificationPage extends StatefulWidget {
  const TestNotificationPage({super.key});

  @override
  State<TestNotificationPage> createState() => _TestNotificationPageState();
}

class _TestNotificationPageState extends State<TestNotificationPage> {
  final NotificationHelper _notificationHelper = NotificationHelper();
  Timer? mTimer;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("定时"),
        centerTitle: true,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                  onPressed: () {
                    _notificationHelper.showNotification(
                      title: 'Hello',
                      body: 'This is a notification!',
                    );
                  },
                  child: const Text("发起通知")),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                  onPressed: () async {
                    alarmConfig();
                    // mTimer = Timer(const Duration(seconds: 5), () {
                    //   _notificationHelper.showNotification(
                    //     title: '3秒后发',
                    //     body: '3秒后发送通知内容',
                    //   );
                    // });
                    // await _notificationHelper.showSchedulNotification(456,'测试',"aaa");
                  },
                  child: const Text("定时通知")),
            ],
          )
        ],
      ),
    );
  }

  void alarmConfig() async {
    final alarmSettings = AlarmSettings(
      id: 42,
      dateTime: DateTime.now().add(const Duration(minutes: 1)),
      assetAudioPath: 'assets/not_blank.mp3',
      loopAudio: false, // 是否循环
      vibrate: false, // 设备会一直震动播放
      volumeMax: true, // 是否最大音量
      fadeDuration: 3.0,
      notificationTitle: 'This is the title',
      notificationBody: 'This is the body',
      enableNotificationOnKill: true,
    );
    await Alarm.set(alarmSettings: alarmSettings);
  }
}
