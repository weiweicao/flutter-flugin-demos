import 'package:flutter/material.dart';
import 'package:fooddemos/theme/app_style.dart';
import 'package:fooddemos/widgets/login_widget.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          Image.asset("assets/images/bg_login_header.png"),
          Column(
            children: [
              const SizedBox(
                height: 320,
              ),
              ClipPath(
                clipper: LoginClipper(),
                child: const LoginBodyWidget(),
              )
            ],
          )
        ],
      ),
    );
  }
}

// 登录页面内容体
class LoginBodyWidget extends StatelessWidget {
  const LoginBodyWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      width: double.maxFinite,
      padding: const EdgeInsets.all(32),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 26,
          ),
          Text(
            'Login',
            style: kTitleTextStyle,
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            'Your Email',
            style: kBodyTextStyle,
          ),
          SizedBox(
            height: 4,
          ),
          LoginInput(
              hintText: "Email", prefixIcon: "assets/icons/icon_email.png"),
          SizedBox(
            height: 16,
          ),
          Text(
            'Your Password',
            style: kBodyTextStyle,
          ),
          SizedBox(
            height: 4,
          ),
          LoginInput(
            hintText: "Password",
            prefixIcon: "assets/icons/icon_pwd.png",
            obscureText: true,
          ),
          SizedBox(height: 12,),
          LoginBtnIconWidget(),
          SizedBox(height: 12,)
        ],
      ),
    );
  }
}
