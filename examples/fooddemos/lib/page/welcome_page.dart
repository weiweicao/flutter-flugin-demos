import 'package:flutter/material.dart';
import 'package:fooddemos/page/login_page.dart';
import 'package:fooddemos/theme/app_colors.dart';
import 'package:fooddemos/widgets/welcome_widget.dart';

class WelcomePage extends StatefulWidget {
  const WelcomePage({super.key});

  @override
  State<WelcomePage> createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kBgColor,
      body: SingleChildScrollView(
        child: Column(
          children: [
            const WelcomeHeaderWidget(),
            GradientBtnWidget(
              width: 208,
              onTap: () {
                Navigator.push(context, MaterialPageRoute(
                  builder: (context) {
                    return LoginPage();
                  },
                ));
              },
              child: const BtnTextWhiteWidget(text: 'Sign up'),
            ),
            const SizedBox(
              height: 16,
            ),
            GestureDetector(
              child: const LoginBtnWidget(),
              onTap: () {},
            ),
            const SizedBox(
              height: 16,
            ),
            const Text(
              'Forget password?',
              style: TextStyle(fontSize: 18, color: kTextColor),
            ),
            const SizedBox(
              height: 24,
            ),
            const Row(
              children: [
                Spacer(),
                LineWidget(),
                LoginTypeIconWidget(
                  icon: "assets/icons/logo_ins.png",
                ),
                LoginTypeIconWidget(
                  icon: "assets/icons/logo_fb.png",
                ),
                LoginTypeIconWidget(
                  icon: "assets/icons/logo_twitter.png",
                ),
                LineWidget(),
                Spacer()
              ],
            ),
          ],
        ),
      ),
    );
  }
}
