import 'dart:math';

import 'package:flutter/material.dart';
import 'package:fooddemos/theme/app_size.dart';
import 'package:fooddemos/theme/app_style.dart';
import 'package:fooddemos/widgets/welcome_widget.dart';

/// 登录页面裁剪曲线
class LoginClipper extends CustomClipper<Path> {
// 第一个点
  Point p1 = const Point(0.0, 54.0);
  Point c1 = const Point(20.0, 25.0);
  Point c2 = const Point(81.0, -8.0);
// 第二个点
  Point p2 = const Point(160.0, 20.0);
  Point c3 = const Point(216.0, 38.0);
  Point c4 = const Point(280.0, 73.0);
  // 第三个点
  Point p3 = const Point(280.0, 44.0);
  Point c5 = const Point(280.0, -11.0);
  Point c6 = const Point(330.0, 8.0);

  @override
  Path getClip(Size size) {
    // 第四个点
    Point p4 = Point(size.width, .0);
    Path path = Path();
    // 移动到第一个点
    path.moveTo(p1.x.toDouble(), p1.y.toDouble());
    // 第一阶段 三阶贝塞尔曲线
    path.cubicTo(c1.x.toDouble(), c1.y.toDouble(), c2.x.toDouble(),
        c2.y.toDouble(), p2.x.toDouble(), p2.y.toDouble());
    // 第二阶段 三阶贝塞尔曲线
    path.cubicTo(c3.x.toDouble(), c3.y.toDouble(), c4.x.toDouble(),
        c4.y.toDouble(), p3.x.toDouble(), p3.y.toDouble());
    // 第三阶段 三阶贝塞尔曲线
    path.cubicTo(c5.x.toDouble(), c5.y.toDouble(), c6.x.toDouble(),
        c6.y.toDouble(), p4.x.toDouble(), p4.y.toDouble());
    // 连接到右下角
    path.lineTo(size.width, size.height);
    //连接到左下角
    path.lineTo(0, size.height);
    //闭合
    path.close();
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    // TODO: implement shouldReclip
    return this.hashCode != oldClipper.hashCode;
  }
}

// 登录输入框
class LoginInput extends StatelessWidget {
  const LoginInput(
      {super.key,
      required this.hintText,
      required this.prefixIcon,
      this.obscureText = false});
  final String hintText;
  final String prefixIcon;
  final bool obscureText;

  @override
  Widget build(BuildContext context) {
    return TextField(
      decoration: InputDecoration(
        hintText: hintText,
        border: kInputBorder,
        focusedBorder: kInputBorder,
        enabledBorder: kInputBorder,
        prefixIcon: Container(
          width: kIconBoxSize,
          height: kIconBoxSize,
          alignment: Alignment.center,
          child: Image.asset(
            prefixIcon,
            width: kIconSize,
            height: kIconSize,
          ),
        ),
      ),
      obscureText: obscureText,
      style: kBodyTextStyle.copyWith(fontSize: 18),
    );
  }
}

/// 登录图标按钮
class LoginBtnIconWidget extends StatelessWidget {
  const LoginBtnIconWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        const Spacer(),
        GradientBtnWidget(
            width: 160,
            child: Row(
              children: [
                const SizedBox(
                  width: 34,
                ),
                const BtnTextWhiteWidget(text: 'Login'),
                const Spacer(),
                Image.asset(
                  "assets/icons/icon_arrow_right.png",
                  width: kIconSize,
                  height: kIconSize,
                ),
                const SizedBox(
                  width: 24,
                ),
              ],
            ),
            onTap: () {
              Navigator.pop(context);
            })
      ],
    );
  }
}
