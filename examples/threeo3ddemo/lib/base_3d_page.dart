/* 
 * @description: 基础应用
 * @author: Jane 
 * @date: 2023-10-16 09:57:07
 */
import 'package:flutter/material.dart';
import 'package:o3d/o3d.dart';
import 'package:threeo3ddemo/line_patiner.dart';
class Base3dPage extends StatefulWidget {
  const Base3dPage({super.key});

  @override
  State<Base3dPage> createState() => _Base3dPageState();
}

class _Base3dPageState extends State<Base3dPage> {
  O3DController controller = O3DController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text("3D展示"),
        actions: [
          IconButton(
              onPressed: () => controller.cameraOrbit(20, 20, 5),
              icon: const Icon(Icons.change_circle)),
          IconButton(
              onPressed: () => controller.cameraTarget(1.2, 1, 4),
              icon: const Icon(Icons.change_circle_outlined)),
        ],
      ),
      body: Stack(
        children: [
          O3D(
            controller: controller,
            src: 'assets/glb/jeff_johansen_idle.glb',
            backgroundColor: Colors.grey,
            poster: "指出来",
            withCredentials: true,
            autoPlay: false, //自动播放
            autoRotate: false, //自动旋转
            autoRotateDelay: 1000, //自动旋转时延
          ),
          // 头部
          Positioned(right: 300, top: 30, child: _lineTip()),
          //身体
          Positioned(
              right: 320,
              top: 210,
              child: _lineTip(str: "身体", dx: 140, dy: 20)),
          //足部
          Positioned(
              right: 310,
              top: 550,
              child: _lineTip(str: "足部", dx: 110, dy: 20)),

          //胳膊
          Positioned(
              right: 10, top: 230, child: _lineTip(str: "胳膊", dx: -75, dy: 20)),
          //腿部
          Positioned(
              right: 10,
              top: 480,
              child: _lineTip(str: "腿部", dx: -105, dy: 20)),
        ],
      ),
    );
  }

  Widget _lineTip({String str = "头部", double dx = 140, double dy = 20}) {
    return Row(
      children: [
        CustomPaint(
          painter: LinePainter(dy: dy, dx: dx),
          child: Container(
            child: Text(str),
          ),
        )
      ],
    );
  }
}
