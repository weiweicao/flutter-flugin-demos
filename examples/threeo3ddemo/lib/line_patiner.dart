/* 
 * @description: 自定义直线
 * @author: Jane 
 * @date: 2023-10-16 14:22:58
 */
import 'package:flutter/material.dart';

class LinePainter extends CustomPainter {
  LinePainter({this.dx = 140, this.dy = 20});
  final double dy; // y轴高度
  final double dx; // x轴起点
  // 定义画笔
  final Paint _paint = Paint()
    ..color = Colors.black
    ..strokeCap = StrokeCap.square
    ..isAntiAlias = true
    ..strokeWidth = 1.0
    ..style = PaintingStyle.stroke;

// 重写绘制内容方法
  @override
  void paint(Canvas canvas, Size size) {
    canvas.drawLine(Offset(0, dy), Offset(dx, dy), _paint);
  }

  @override
  bool shouldRepaint(LinePainter oldDelegate) => false;
}
