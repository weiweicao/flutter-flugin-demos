// import 'package:flutter/material.dart';

// void main() {
//   runApp(const MyApp());
// }

// class MyApp extends StatelessWidget {
//   const MyApp({super.key});

//   // This widget is the root of your application.
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Flutter Demo',
//       theme: ThemeData(
//         colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
//         useMaterial3: false,
//       ),
//       home:Scaffold(
//         appBar: AppBar(title: Text("搜素"),),
//         body:  LayoutDemo(),
//       ),
//     );
//   }
// }

// class LayoutDemo extends StatelessWidget {
//   LayoutDemo({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Padding(
//       padding: const EdgeInsets.all(10.0),
//       child: Wrap(
//         alignment: WrapAlignment.spaceAround,
//         spacing: 10,// 水平间距
//         runSpacing: 10,// 垂直间距
//         direction: Axis.horizontal,
//         children: [
//           Button("第1集", onPressed: () {}),
//           Button("第2集", onPressed: () {}),
//           Button("第3集", onPressed: () {}),
//           Button("第4集", onPressed: () {}),
//           Button("第5集", onPressed: () {}),
//           Button("第6集", onPressed: () {}),
//           Button("第7集", onPressed: () {}),
//           Button("第8集", onPressed: () {}),
//           Button("第9集", onPressed: () {}),
//           Button("第10集", onPressed: () {}),
//           Button("第11集", onPressed: () {}),
//           Button("第12集", onPressed: () {}),
//           Button("第13集", onPressed: () {}),
//           Button("第14集", onPressed: () {}),
//           Button("第15集", onPressed: () {}),
//           Button("第16集", onPressed: () {}),
//           Button("第17集", onPressed: () {}),
//           Button("第18集", onPressed: () {}),
//           Button("第19集", onPressed: () {}),
//         ],
//       ),
//     );
//   }
// }

// // 自定义按钮组件
// class Button extends StatelessWidget {
//   String text; // 按钮文字
//   void Function()? onPressed; // 方法
//   Button(this.text, {Key? key, required this.onPressed}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return ElevatedButton(
//         style: ButtonStyle(
//           backgroundColor: MaterialStateProperty.all(Colors.grey),
//           foregroundColor: MaterialStateProperty.all(Colors.black54),
//         ),
//         onPressed: onPressed,
//         child: Text(text));
//   }
// }
