import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getxdemo/controllers/count_controller.dart';

import 'language.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
      translations: Messages(), // your translations
      locale:
          Locale('zh', 'CN'), // translations will be displayed in that locale
      fallbackLocale: Locale('en',
          'UK'), // specify the fallback locale in case an invalid locale is selected.
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  CountControler countControler = Get.put(CountControler());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          // TRY THIS: Try changing the color here to a specific color (to
          // Colors.amber, perhaps?) and trigger a hot reload to see the AppBar
          // change color while the other colors stay the same.
          backgroundColor: Theme.of(context).colorScheme.inversePrimary,
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: Text(widget.title),
        ),
        body: Center(
            // Center is a layout widget. It takes a single child and positions it
            // in the middle of the parent.
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Obx(() => Text("${countControler.couter.value}")),
            Text('hello'.tr),
            const SizedBox(
              height: 40,
            ),
            ElevatedButton(
                onPressed: () {
                  countControler.inc();
                },
                child: const Text("计数器加1"))
          ],
        )));
  }
}
