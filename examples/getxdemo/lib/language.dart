/* 
 * @description: 多语言
 * @author: Jane 
 * @date: 2023-11-09 15:12:51
 */

import 'package:get/get.dart';

class Messages extends Translations {
  @override
  // TODO: implement keys
  Map<String, Map<String, String>> get keys => {
        'zh_CN': {'hello': '你好世界', 'title': '标题'},
        'en_US': {'hello': 'Hallo Welt', 'title': 'title'}
      };
}
