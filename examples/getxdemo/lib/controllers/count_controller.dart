import 'package:get/get.dart';

class CountControler extends GetxController {
  RxInt couter = 0.obs;
 void  inc() {
    couter.value++;
    update();
  }

 void dec() {
    couter.value--;
    update();
  }
}
