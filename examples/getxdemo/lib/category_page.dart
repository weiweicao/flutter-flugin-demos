import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getxdemo/controllers/count_controller.dart';

class CategoryPage extends StatefulWidget {
  const CategoryPage({super.key});

  @override
  State<CategoryPage> createState() => _CategoryPageState();
}

class _CategoryPageState extends State<CategoryPage> {
  // 获取计数器实例
  CountControler countControler = Get.find();

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Obx(() => Text(
                "${countControler.couter}",
                style: Theme.of(context).textTheme.headlineMedium,
              ))
        ],
      ),
    );
  }
}
